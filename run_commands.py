#!/usr/bin/env python

import subprocess

def run_command(command):
    process = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
    output, error = process.communicate()
    print(output.decode())

run_command("pip install virtualenv")
run_command("virtualenv venv")
run_command("source venv/bin/activate")
run_command("pip install -r requirements.txt")
run_command("python app.py")
