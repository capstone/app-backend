# Running the solution steps.
- Open a terminal window and navigate to the directory where the script file "run_commands.py" for running this solution is saved.
- Make the script file "run_commands.py" is executable by running the following command: chmod +x run_commands.py
- Finally, run the script by typing the following command in the terminal: ./run_commands.py

## Run these commands:
- chmod +x run_commands.py
- ./run_commands.py

## What will running these steps do

- This script will install virtualenv, create a virtual environment named venv, activate the virtual environment, install the packages listed in the requirements.txt file, and run the app.py script.
- Note that you need to have Python installed on your system to run this script. Also, make sure that you have a requirements.txt file in the same directory as the script, otherwise the pip install -r requirements.txt command will fail.

## Command Line
- virtualenv venv (if virtualenv is not installed, run pip install virtualenv before this step)
- source ./venv/bin/activate
- pip install -r requirement.txt
- python app.py

