import os

def upload_folder():
    #Create path to save the file inside user_id
    dirname = os.path.dirname
    root_dir = (dirname(dirname(os.path.dirname(__file__))))
    return os.path.join(root_dir, 'static')

def display_pic_upload_folder():
    #Create path to save the file inside user_id
    dirname = os.path.dirname
    root_dir = (dirname(dirname(os.path.dirname(__file__))))
    return os.path.join(root_dir, 'static/display_pic')
    
class Config(object):
    #Use this to create database.
    SQLALCHEMY_DATABASE_URI = ""
    if (os.getenv("RUN_IN_CLOUD", False)):
        SQLALCHEMY_DATABASE_URI = f"mysql+pymysql://root:megamind@app-backend-mysql.capstone.svc.cluster.local/capstoneMegamind"
        # SQLALCHEMY_DATABASE_URI = f"mysql+pymysql://root:megamind@2001-0468-0c80-2108-0001-7010-665b-7e30.sslip.io/capstoneMegamind"
    else:
        SQLALCHEMY_DATABASE_URI = 'sqlite:///registration.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # TO DO : Generate and write secret key out of the app.
    JWT_SECRET_KEY = 'temp'
    MAX_CONTENT_LENGTH = 2 * 1024 * 1024
    UPLOAD_EXTENSIONS = ['.jpg', '.png']
    UPLOAD_PATH = upload_folder()
    UPLOAD_FOLDER = display_pic_upload_folder()
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 587
    MAIL_USERNAME = 'megaminds.capstone@gmail.com'
    MAIL_PASSWORD = 'dyedjqfqgecrlkng'
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    MAIL_DEFAULT_SENDER = 'megaminds.capstone@gmail.com'
    TWILIO_SID = 'AC05f3719083ed8805aafc3aaab8f1952e'
    TWILIO_TOKEN = 'fdc9ca4f2fb8a912ed19875a780db0be'
    TWILIO_PHONE_NUMBER = '+18556471094'
    REPORTED_COUNT_THRESHOLD=5