from app import app, db, mail, jwt
import uuid
from flask import request, url_for, redirect, render_template

from app.model.Verification import Verification
from app.model.user import *
from app.model.product import *
from app.model.rating import *
from flask import jsonify, send_from_directory
from werkzeug.security import generate_password_hash, check_password_hash
from werkzeug.utils import secure_filename
import datetime as dt
from datetime import datetime, timedelta
from flask_jwt_extended import (
    jwt_required, create_access_token, get_jwt_identity,
    unset_jwt_cookies
)
from app.model.reporting import *
import os
from flask_mail import Message
from twilio.rest import Client
import shutil
import random
import smtplib
import string


@app.route('/')
@app.route('/health')
def health():
    '''
    Health endpoint
    '''
    return f"Hello Megaminds"


# login
@app.route('/')
@app.route("/login", methods=["POST"])
def login():
    username = request.json.get('username')
    password = request.json.get('password')
    if username is None or password is None:
        return jsonify({'message': 'Missing Arguments'}), 400

    user = User.query.filter_by(username=username).first()
    if user is None or not check_password_hash(user.password, password):
        return jsonify({'message': 'Wrong Credentials'}), 400
    if not user.is_active and user.reported_count > app.config['REPORTED_COUNT_THRESHOLD']:
        return jsonify({'message': 'Your profile has been reported more than 5 times. Please contact the admin at megaminds.capstone@gmail.com . Thank you!'}), 500
    if not user.is_active:
        return jsonify({'message': 'User has not verified his/her profile', 'phone': user.phone, 'email': user.email,}), 200
    accessToken = create_access_token(
        identity=username, expires_delta=dt.timedelta(hours=1))
    return jsonify(message="Login sucessfull", access_token=accessToken)


@app.route('/logout', methods=['POST'])
@jwt_required()
def logout():
    resp = jsonify({'logout': True})
    unset_jwt_cookies(resp)
    return resp, 200


@app.route('/profile', methods=['GET'])
@jwt_required()
def user_profile():
    '''
    API to fetch profile details of the user. Would fetch the average buyer and seller rating from Rating table and would update user table using these two entries.
    '''
    username = request.args.get('username')
    usernameFromJWT = get_jwt_identity()
    if (username != usernameFromJWT):
        return jsonify({'message': 'Incorrect username, please re-enter'}), 400
    userDetails = User.query.filter_by(username=username).first()
    if userDetails:
        result = userSchema.dump(userDetails)
        return jsonify(result), 200
    else:
        return jsonify({'message': 'User details not found for username: ' + username}), 404


@app.route('/profile', methods=['DELETE'])
@jwt_required()
def delete_profile():
    '''
    API to fetch delete profile of user.
    '''
    username = get_jwt_identity()
    userDetails = User.query.filter_by(username=username).first()
    if userDetails:
        try:
            # Delete all ratings associated with the user
            Rating.query.filter_by(username=username).delete()

            # Delete all products associated with the user
            Product.query.filter_by(seller_id=userDetails.id).delete()

            # Delete the user
            db.session.delete(userDetails)
            db.session.commit()

            # DELETE all the product images / Profile
            if (userDetails.profile_image and os.path.exists(
                    os.path.join(app.config['UPLOAD_FOLDER'], userDetails.profile_image))):
                os.remove(os.path.join(app.config['UPLOAD_FOLDER'], userDetails.profile_image))

            # DELETE USER FOLDER with product images
            if (os.path.exists(os.path.join(app.config['UPLOAD_PATH'], str(userDetails.id)))):
                shutil.rmtree(os.path.join(app.config['UPLOAD_PATH'], str(userDetails.id)))

            return jsonify({'message': f'User Deleted: {username}'}), 201

        except Exception as e:
            db.session.rollback()
            print(e)
            return jsonify({'message': 'Failed to delete user'}), 500
    else:
        return jsonify({'message': f'User details not found for Delete: {username}'}), 404


def calculateAvgRating(username, buyer=False, seller=False):
    '''
    Function to calculate avergage buyer/seller rating of a user with the help of Rating table. Formula used : Sum of ratings/Number of raters
    '''

    try:
        if buyer is True:
            query = Rating.query.filter(Rating.username == username)
            for row in query:
                if row.no_of_buyer_raters == 0:
                    return 0
                division = row.buyer_rating_sum / row.no_of_buyer_raters
                return division
        if seller is True:
            query = Rating.query.filter(Rating.username == username)
            result = []
            for row in query:
                if row.no_of_seller_raters == 0:
                    return 0
                division = row.seller_rating_sum / row.no_of_seller_raters
                return division
    except Exception as e:
        return jsonify(message="Request failed"), 500


@app.route('/register', methods=['POST'])
def register():
    '''
    API to register user, makes a corresponding entry in Rating table as well. 
    '''
    username = request.json['username']
    password = request.json['password']
    phone = request.json['phone']
    email = request.json['email']
    college = request.json['college']

    try:
        if not email.endswith("@vt.edu"):
            return jsonify(message="Please register with a vt domain email address"), 400
        existingUser = User.query.filter_by(username=username).first()
        existingPhone = User.query.filter_by(phone=phone).first()
        existingEmail = User.query.filter_by(email=email).first()
        if (existingUser):
            return jsonify({'message': 'Username already exists'}), 400
        elif existingEmail:
            return jsonify({'message': 'User with the same email exists'}), 400
        elif existingPhone:
            return jsonify({'message': 'User with the same phone exists'}), 400
        else:
            hash_password = generate_password_hash(password)
            user = User(username=username, password=hash_password, email=email, phone=phone, 
                        avg_buyer_rating=0, avg_seller_rating=0, college=college)
            db.session.add(user)
            db.session.commit()
            user_data = User.query.filter_by(username=username).first()
            rating = Rating(buyer_rating_sum=0, no_of_buyer_raters=0,
                            seller_rating_sum=0, no_of_seller_raters=0, username=username)
            db.session.add(rating)
            db.session.commit()
            if user_data:
                msg = Message("Account created successfully on OnTheBlock with username :" +
                              username, sender="megaminds.capstone@gmail.com", recipients=[email])
                mail.send(msg)
                accessToken = create_access_token(
                    identity=username, expires_delta=dt.timedelta(hours=1))
                return jsonify(message="Login sucessfull", access_token=accessToken)
    except Exception as e:
        return jsonify({'Error occured while adding rating : ': str(e)}), 400


@app.route("/products", methods=["GET"])
def get_products():
    '''
    get products,
    sample curl --> curl http://localhost:5000/products?id=1 for id 1 or
    curl http://localhost:5000/products?category=electronics for products in the electronics category or
    curl http://localhost:5000/products for all
    '''
    product_id = request.args.get('id')
    category = request.args.get('category')
    if product_id:
        product = Product.query.get(product_id)
        if product:
            return jsonify({'product': product.as_dict()})
        else:
            return jsonify({'message': 'Product not found'}), 404
    elif category:
        products = Product.query.filter_by(category=category).all()
        if products:
            products = [product.as_dict() for product in products]
            return jsonify({'products': products})
        else:
            return jsonify({'message': 'No products found in the specified category'}), 404
    else:
        products = Product.query.all()
        products = [product.as_dict() for product in products]
        return jsonify({'products': products})


@app.route('/product_post', methods=['POST'])
@jwt_required()
def post_product():
    '''
    post products 
    (product details get input by user and api will save product data into DB), 
    Example :
    curl -v -X POST -H "Authorization: Bearer $ACCESS_TOKEN" -H "Content-Type: application/json" -d '{"name":"test-product", "description" : "testing product", "price" : "10", "category" : "furniture", "condition" : "new",  "market_available_date" : "2023-03-28", "seller_id" : "3", "location" : "Blacksburg" }' localhost:8080/product_post
    '''

    usernameFromJWT = get_jwt_identity()
    userDetails = User.query.filter_by(username=usernameFromJWT).first()
    name = request.json['name']
    description = request.json['description']
    # run image upload API endpoint to upload images
    price = request.json['price']
    category = request.json['category']
    condition = request.json['condition']
    location = request.json['location']
    created_date = datetime.today().date()
    market_available_date = datetime.strptime(
        request.json['market_available_date'], '%Y-%m-%d')
    # seller_id = request.json['seller_id']
    seller_id = userDetails.id

    try:
        if name is None or price is None or seller_id is None:
            return jsonify({'message': 'post_product() name cannot be null: '}), 400
        elif price is None:
            return jsonify({'message': 'post_product() price cannot be null: '}), 400
        elif seller_id is None:
            return jsonify({'message': 'post_product() seller_id cannot be null: '}), 400

        # Commented out below code because products should not have unique product names. unique identifier should stick with id

        # existingProductName = Product.query.filter_by(name=name).first()
        # if(existingProductName):
        #     return jsonify(message="Product Name already exists"), 200
        # else:

        product = Product(name=name, description=description,
                          price=price, category=category, condition=condition,
                          location=location, created_date=created_date,
                          market_available_date=market_available_date,
                          seller_id=seller_id)
        db.session.add(product)
        db.session.commit()

        product_data = Product.query.filter_by(id=product.id).first()
        if product_data:
            return jsonify(
                message=f"Product has been saved successfully: product_id = {product.id}, name = {product.name}",
                product_id=product.id)

    except Exception as e:
        return e


# return user id from username

@app.route('/product_update', methods=['PUT'])
@jwt_required()
def update_product():
    '''
    update products 
    (product details get changed by user and api will update product data into DB), 
    Example :
    curl -v -X PUT -H "Authorization: Bearer $ACCESS_TOKEN" -H "Content-Type: application/json" -d '{"id":"3","name":"test-product", "description" : "testing product", "price" : "10", "category" : "furniture", "condition" : "new",  "market_available_date" : "2023-03-28", "location" : "Blacksburg" }' localhost:8080/product_update
    '''

    usernameFromJWT = get_jwt_identity()
    userDetails = User.query.filter_by(username=usernameFromJWT).first()
    product_id = request.json["id"]
    name = request.json['name']
    description = request.json['description']
    price = request.json['price']
    category = request.json['category']
    condition = request.json['condition']
    location = request.json['location']
    created_date = datetime.today().date()
    market_available_date = datetime.strptime(
        request.json['market_available_date'], '%Y-%m-%d')
    seller_id = userDetails.id

    try:

        if name is None or price is None or seller_id is None:
            return jsonify({'message': 'update_product() name cannot be null: '}), 400
        elif price is None:
            return jsonify({'message': 'update_product() price cannot be null: '}), 400
        elif seller_id is None:
            return jsonify({'message': 'update_product() seller_id cannot be null: '}), 400

        product_data = Product.query.filter_by(id=product_id).first()

        if product_data is None:
            return jsonify({'message': 'update_product() product does not exist in DB: '}), 400

        if seller_id != product_data.seller_id:
            return jsonify({'message': 'update_product() seller_id does not match with the product seller_id: '}), 400

        product_data.name = name
        product_data.description = description
        product_data.price = price
        product_data.category = category
        product_data.condition = condition
        product_data.location = location
        product_data.created_date = created_date
        product_data.market_available_date = market_available_date

        db.session.commit()
        return {'message': 'update_product() product updated successfully', 'product_id': product_data.id}

    except Exception as e:
        return e


@app.route('/product_mark_sold', methods=['PUT'])
@jwt_required()
def mark_product_sold():
    '''
    mark product as sold 
    (product gets marked as sold by user and api will update product column "is_sold" from false to true into DB), 
    Example :
    curl -v -X PUT -H "Authorization: Bearer $ACCESS_TOKEN" -H "Content-Type: application/json" -d '{"id":"3", "is_sold": true }' localhost:8080/product_update
    '''
    usernameFromJWT = get_jwt_identity()
    seller_id = get_user_id(usernameFromJWT)
    product_id = request.json["id"]

    try:
        if seller_id is None:
            return jsonify({'message': 'mark_product_sold() seller_id cannot be null: '}), 400

        product_data = Product.query.filter_by(id=product_id).first()

        if product_data is None:
            return jsonify({'message': 'mark_product_sold() product does not exist in DB: '}), 400

        if seller_id != product_data.seller_id:
            return jsonify(
                {'message': 'mark_product_sold() seller_id does not match with the product seller_id: '}), 400

        if not product_data.is_sold:
            product_data.is_sold = True
        else:
            return jsonify({'message': 'mark_product_sold() product is already sold: '}), 400

        db.session.commit()
        return {'message': 'mark_product_sold() product updated and marked as sold successfully',
                'product_id': product_data.id}

    except Exception as e:
        return e


def get_user_id(username):
    return User.query.filter_by(username=username).first().id


@app.route('/upload_files', methods=['POST'])
@jwt_required()
def upload_files():
    '''
    Function to upload multi files.
    Can be tested using :
    curl -vvv -X POST -H "Authorization: Bearer $ACCESS_TOKEN"  -F file=@check.jpg  -F file=@test2.jpg  "http://localhost:8080/upload_files?product_id=3"
    '''
    file_list = request.files.getlist('file')
    for uploaded_file in file_list:
        filename = secure_filename(uploaded_file.filename)
        if filename != '':
            file_ext = os.path.splitext(filename)[1]
            if file_ext not in app.config['UPLOAD_EXTENSIONS']:
                return jsonify({"message": "Invalid image"}), 400

    user_name = get_jwt_identity()
    user_id = str(get_user_id(user_name))

    # we save all static contenct for user inside user_id folder and then inside product
    # check if path exist for user_id to save the files if not crate
    # Ex. path becomes static/user_id/product_id
    # TO DO: replace this with product_id for product it is being uploaded
    product_id = request.form.get('product_id')

    if product_id is None or product_id.isnumeric() != True:
        return jsonify({"message": "Please send product id, (expects number)"}), 400

    upload_folder = app.config['UPLOAD_PATH']

    # create static if not exists.
    if os.path.isdir(upload_folder) != True:
        os.mkdir(upload_folder)

    print(os.path.join(upload_folder, user_id))
    if os.path.isdir(os.path.join(upload_folder, user_id)) != True:
        os.makedirs(os.path.join(upload_folder, user_id, product_id))
    elif os.path.isdir(os.path.join(upload_folder, user_id, product_id)) != True:
        os.mkdir(os.path.join(upload_folder, user_id, product_id))
        print(f"Succesfully created dir, {user_id}")

    # overwrite upload_folder now till the actual location
    upload_folder = os.path.join(upload_folder, user_id, product_id)
    print(f"Uploading inside folder : {upload_folder}")
    for uploaded_file in file_list:
        uploaded_file.save(os.path.join(upload_folder, uploaded_file.filename))

    return jsonify({
                       "message": f"Files have been uploaded successfully: for user_name = {user_name}, files = {[f.filename for f in file_list]}"}), 204


@app.route("/list_files", methods=["GET"])
def retrieve_images():
    '''
    Endpoint retrieve images for the product id belonging to the seller/user
    Endpoint return list of files, then /get_file function must be called 
    with filenames retrieve from this endpoint to get individual files.
    curl -vvv -H "Authorization: Bearer $ACCESS_TOKEN"  "http://localhost:8080/list_files?product_id=3&user_id=6"
    '''

    user_id = request.args.get('user_id', type=int)
    product_id = request.args.get('product_id', type=int)

    if product_id is None or user_id is None:
        return jsonify({"message": "Product/User ID is not sent"}), 400

    product_id = str(product_id)
    user_id = str(user_id)
    product_path = os.path.join(app.config['UPLOAD_PATH'], user_id, product_id)

    if os.path.isdir(product_path) != True:
        return jsonify({"message": f"No images exist of product : {product_id}, for user : {user_id} "}), 500

    print(os.listdir(product_path))
    return os.listdir(product_path), 200


@app.route("/get_file", methods=["GET"])
def get_file():
    '''
    Download file, get the file name from list_files endpoint
    curl -vvv -H "Authorization: Bearer $ACCESS_TOKEN"  "http://localhost:8080/get_file?user_id=3&product_id=3&file_name=screen.png" -o screen.png
    '''

    user_id = request.args.get('user_id', type=int)
    product_id = request.args.get('product_id', type=int)

    if product_id is None or user_id is None:
        return jsonify({"message": "Product | User ID is not sent"}), 400

    file_name = request.args.get('file_name')
    if file_name is None:
        return jsonify({"message": "File Name to retrieve is not sent"}), 400

    product_id = str(product_id)
    user_id = str(user_id)
    product_path = os.path.join(app.config['UPLOAD_PATH'], user_id, product_id)

    if os.path.isdir(product_path) != True:
        return jsonify({"message": f"No  images exist of product : {product_id}, for user : {user_id} "}), 500

    if os.path.isfile(os.path.join(product_path, file_name)) != True:
        return jsonify(
            {"message": f"No image named : {file_name} exist of product : {product_id}, for user : {user_id} "}), 500

    return send_from_directory(product_path, file_name)


@app.route('/delete_file', methods=['DELETE'])
@jwt_required()
def delete_file():
    '''
    Delete file based on user id, product id, and filename
    curl -vvv -H "Authorization: Bearer $ACCESS_TOKEN"  "http://localhost:8080/delete_file?user_id=3&product_id=3&file_name=screen.png" -o screen.png
    '''
    usernameFromJWT = get_jwt_identity()
    userDetails = User.query.filter_by(username=usernameFromJWT).first()
    user_id = userDetails.id
    product_id = request.args.get('product_id', type=int)

    if product_id is None or user_id is None:
        return jsonify({"message": "Product | User ID is not sent"}), 400

    file_name = request.args.get('file_name')
    if file_name is None:
        return jsonify({"message": "File Name to retrieve is not sent"}), 400

    product_id = str(product_id)
    user_id = str(user_id)
    product_path = os.path.join(app.config['UPLOAD_PATH'], user_id, product_id)

    if os.path.isdir(product_path) != True:
        return jsonify({"message": f"No  images exist of product : {product_id}, for user : {user_id} "}), 500

    if os.path.isfile(os.path.join(product_path, file_name)) != True:
        return jsonify(
            {"message": f"No image named : {file_name} exist of product : {product_id}, for user : {user_id} "}), 500

    full_file_path = os.path.join(product_path, file_name)

    try:
        os.remove(full_file_path)
        return jsonify(
            message=f"Image deleted name : {file_name} with product_id = {product_id} and for user with id = {user_id}")
    except OSError as e:
        return jsonify({
                           "message": f"OSError deleting image named : {file_name} with product id : {product_id}, for user : {user_id} "}), 500


@app.route("/rating", methods=["POST"])
@jwt_required()
def rate():
    '''
    API to rate buyers or sellers. 

    If I am a user and I want to rate username 'thakurv1' with a buyer_rating of 5 and seller_rating of 5, I will use the below JSON body: 

    curl --location --request POST 'http://127.0.0.1:5000/rating' \
    --header 'Content-Type: application/json' \
    --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmcmVzaCI6ZmFsc2UsImlhdCI6MTY3OTk3MjE4NywianRpIjoiMGU0MzY2OWMtMmIxNS00MzA0LWEzZjAtNjE0ZDA1MzA0YWYyIiwidHlwZSI6ImFjY2VzcyIsInN1YiI6InRoYWt1cnYyIiwibmJmIjoxNjc5OTcyMTg3LCJleHAiOjE2Nzk5NzU3ODd9.0F1lMD9nilBmgrHnQAB4kMPoeqkQ0PkH0Jgz-1GKF9c' \
    --data-raw '{

        "seller_rating":3,
        "username":"thakurv2",
        "buyer_rating":3
    }'

    We can pass either one of seller_rating or buyer_rating or both. 

    '''
    if request.json.get('buyer_rating') is not None:
        buyer_rating = request.json.get('buyer_rating')
        if buyer_rating < 0 or buyer_rating > 5:
            return jsonify(message="Rating value should be an integer between 0 and 5"), 400
        buyer_raters = 1
    else:
        buyer_rating = 0
        buyer_raters = 0

    if request.json.get('seller_rating') is not None:
        seller_rating = request.json.get('seller_rating')
        if seller_rating < 0 or seller_rating > 5:
            return jsonify(message="Rating value should be an integer between 0 and 5"), 400
        seller_raters = 1
    else:
        seller_rating = 0
        seller_raters = 0
    userId = request.json['username']
    user = User.query.get(userId)
    username = user.username
    if user is None:
        return jsonify(message="username does not exist, please try again"), 400
    try:
        rating = Rating.query.filter_by(username=username).first()
        if not rating:
            rating = Rating(buyer_rating_sum=buyer_rating, no_of_buyer_raters=buyer_raters,
                            seller_rating_sum=seller_rating, no_of_seller_raters=seller_raters, username=username)
            db.session.add(rating)
            db.session.commit()
            user.avg_buyer_rating = calculateAvgRating(username=username,
                                                       buyer=True)
            user.avg_seller_rating = calculateAvgRating(username=username,
                                                        seller=True)
            db.session.commit()
            return jsonify(message="Rating added successfully"), 201
        else:
            rating.buyer_rating_sum = rating.buyer_rating_sum + buyer_rating
            rating.no_of_buyer_raters = rating.no_of_buyer_raters + buyer_raters
            rating.seller_rating_sum = rating.seller_rating_sum + seller_rating
            rating.no_of_seller_raters = rating.no_of_seller_raters + seller_raters
            db.session.commit()
            user.avg_buyer_rating = calculateAvgRating(username=username,
                                                       buyer=True)
            user.avg_seller_rating = calculateAvgRating(username=username,
                                                        seller=True)
            db.session.commit()
            return jsonify(message="Rating added successfully"), 200
    except Exception as e:
        db.session.rollback()
        return jsonify({'Error occured while adding rating : ': str(e)})


@app.route('/product/<int:product_id>', methods=['DELETE'])
@jwt_required()
def delete_product(product_id):
    """
    delete a product by id
    """

    username = get_jwt_identity()

    user = User.query.filter_by(username=username).first()
    product = Product.query.filter_by(id=product_id).first()

    if product is None:
        return jsonify({'message': 'Product not found'}), 404

    if product.seller_id != user.id:
        return jsonify({'message': 'You are not authorized to delete this product'}), 401

    db.session.delete(product)
    db.session.commit()

    product_path = os.path.join(app.config['UPLOAD_PATH'], str(user.id), str(product_id))

    if os.path.exists(product_path):
        for file_name in os.listdir(product_path):
            file_path = os.path.join(product_path, file_name)
            os.remove(file_path)
        os.rmdir(product_path)

    return jsonify({'message': 'Product deleted successfully'}), 200


def allowed_file(filename):
    # configure allowed file types for profile picture upload
    ALLOWED_EXTENSIONS = {'jpg', 'jpeg', 'png', 'gif'}
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


# Create a route to upload user profile image
'''
  curl -X POST \
  -H "Authorization: Bearer <access_token>" \
  -F "profile_image=@/path/to/image.jpg" \
  http://localhost:5000/user/<username>/profile-image

  '''


@app.route('/user/profile-image', methods=['POST'])
@jwt_required()
def upload_user_profile_image():
    username = get_jwt_identity()
    user = User.query.filter_by(username=username).first()

    if not user:
        return jsonify({'error': 'User not found'}), 404

    # check if the post request has the file part
    if 'profile_image' not in request.files and 'profile_image' not in request.form:
        return jsonify({'error': 'No file part in the request'}), 400

    if 'profile_image' in request.files:
        file = request.files['profile_image']
        if file.filename == '':
            return jsonify({'error': 'No selected file'}), 400
        if not allowed_file(file.filename):
            return jsonify({'error': 'Invalid file type'}), 400
        filename_full = secure_filename(file.filename)
        filename = os.path.splitext(filename_full)[0]
        file_ext = os.path.splitext(filename_full)[1]
        unique_filename = f"{str(filename)}_{uuid.uuid4().hex}{file_ext}"
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], unique_filename))
        user.profile_image = unique_filename
        db.session.commit()
        return jsonify(userSchema.dump(user)), 200

    elif 'profile_image' in request.form:
        file_path = request.form['profile_image']
        if not allowed_file(file_path):
            return jsonify({'error': 'Invalid file type'}), 400
        filename_full = secure_filename(os.path.basename(file_path))
        filename = os.path.splitext(filename_full)[0]
        file_ext = os.path.splitext(filename_full)[1]
        unique_filename = f"{str(filename)}_{uuid.uuid4().hex}{file_ext}"
        shutil.copyfile(file_path, os.path.join(app.config['UPLOAD_FOLDER'], unique_filename))
        user.profile_image = unique_filename
        db.session.commit()
        return jsonify(userSchema.dump(user)), 200

    return jsonify({'error': 'Error Uploading file'}), 400


@app.route('/user/profile-image', methods=['GET'])
@jwt_required()
def get_user_profile_image():
    username = get_jwt_identity()
    user = User.query.filter_by(username=username).first()

    if not user:
        return jsonify({'error': 'User not found'}), 404

    if not user.profile_image:
        return jsonify({'message': 'No profile image found for user'}), 404

    profile_image_path = os.path.join(app.config['UPLOAD_FOLDER'], user.profile_image)
    profile_image_folder = os.path.dirname(profile_image_path)

    if not os.path.exists(profile_image_path):
        return jsonify({'message': 'Profile image not found on server'}), 404

    return send_from_directory(profile_image_folder, user.profile_image)


@app.route('/product/<int:product_id>/seller-contact', methods=['GET'])
def get_seller_contact(product_id):
    product = Product.query.filter_by(id=product_id).first()
    if not product:
        return jsonify({'error': 'Product not found'}), 404

    seller = User.query.filter_by(id=product.seller_id).first()
    if not seller:
        return jsonify({'error': 'Seller not found'}), 404

    return jsonify({'seller_contact': seller.as_dict()}), 200


@app.route('/forgot-password', methods=['POST'])
def forgot_password():
    email = request.json['email']

    # Check if user with provided email exists
    user = User.query.filter_by(email=email).first()
    if not user:
        return jsonify({'error': 'User not found'}), 404

    # Generate a token with user's id and expiration time of 1 hour
    token = create_access_token(
        identity=user.username, expires_delta=dt.timedelta(hours=1))

    # Create a reset password link with the token
    reset_password_link = 'http://localhost:3000/reset-password?token=' + token + '&username=' + user.username

    # Send email with reset password link to user
    msg = Message("Reset your password on OnTheBlock",
                  sender="megaminds.capstone@gmail.com", recipients=[email])
    msg.body = f"To reset your password, please click the following link: {reset_password_link}"
    mail.send(msg)

    # Redirect user to reset password form
    return jsonify({'message': 'Reset password link sent to your email'}), 200


# Route for resetting password
@app.route('/reset-password', methods=['POST'])
def reset_password():
    token = request.form['token']
    password = request.form['password']
    confirm_password = request.form['confirm_password']

    try:
        # Verify token and extract username
        username = request.form['username']
    except:
        return jsonify({'error': 'Invalid token'}), 400

    # Check if user with extracted username exists
    user = User.query.filter_by(username=username).first()
    if not user:
        return jsonify({'error': 'User not found'}), 404

    # Verify that password and confirm_password match
    if password != confirm_password:
        return jsonify({'error': 'Passwords do not match'}), 400

    # Set new password for user
    user.password = generate_password_hash(password)

    # Save changes to database
    db.session.commit()

    return jsonify({'message': 'Password reset successfully'}), 200


@app.route("/seller_products", methods=["GET"])
@jwt_required()
def get_seller_products():
    username = get_jwt_identity()
    seller_id = str(get_user_id(username))
    # seller_id = request.args.get('id')
    if seller_id:
        products = Product.query.filter_by(seller_id=seller_id).all()
        if products:
            products = [product.as_dict() for product in products]
            return jsonify({'products': products})
        else:
            return jsonify({'product': []})
    else:
        return jsonify({'message': 'Seller has not uploaded'}), 404


@app.route('/send-email', methods=['POST'])
@jwt_required()
def send_email():
    # Get buyer's name, email, and message from request
    buyer_name = request.form['buyer_name']
    buyer_email = request.form['buyer_email']
    message = request.form['message']

    # Get seller's email from database
    seller_email = request.form['seller_email']

    # Create email message
    subject = f"New message from {buyer_name}"
    body = f"Buyer's email: {buyer_email}\n\nMessage: {message}"
    msg = Message(subject=subject, recipients=[seller_email], body=body)

    # Send email
    mail.send(msg)

    return jsonify({'message': 'Email sent successfully'}), 200


@app.route('/2fa-phone', methods=['POST'])
def enable_2fa_phone():
    phone_number = request.json['phone_number']

    # Generate a verification code of 6 random alphanumeric characters
    verification_code = ''.join(random.choices(string.ascii_uppercase + string.digits, k=6))
    # Set expiration time for verification code to 10 minutes from now
    expiration_time = datetime.utcnow() + timedelta(minutes=10)
    # Save the verification code to the database
    verification = Verification(phone=phone_number, code=verification_code, expires_at=expiration_time)

    twilio_sid = app.config['TWILIO_SID']
    twilio_token = app.config['TWILIO_TOKEN']

    # Send verification code to user via SMS
    client = Client(twilio_sid, twilio_token)
    message = client.messages.create(
        body=f"Your verification code is {verification_code}",
        from_=app.config['TWILIO_PHONE_NUMBER'],
        to=phone_number
    )

    # Save the verification code to the database
    db.session.add(verification)
    db.session.commit()

    return jsonify({'message': f'Verification code sent to {phone_number}'}), 200


@app.route('/2fa-email', methods=['POST'])
def enable_2fa_email():
    email = request.json.get('email')
    if not email:
        return jsonify({'error': 'Email address is required'}), 400

    # Check if user with provided email exists
    user = User.query.filter_by(email=email).first()
    if not user:
        return jsonify({'error': 'User not found'}), 404

    # Generate a verification code of 6 random alphanumeric characters
    verification_code = ''.join(random.choices(string.ascii_uppercase + string.digits, k=6))
    expiration_time = datetime.utcnow() + timedelta(minutes=10)

    # Send verification code to user via email
    msg = Message("2-Factor Authentication Verification Code",
                  sender=app.config['MAIL_USERNAME'], recipients=[email])
    msg.body = f"Your verification code is {verification_code}"
    mail.send(msg)

    db.session.add(Verification(email=user.email, code=verification_code, expires_at=expiration_time))
    db.session.commit()

    return jsonify({'message': f'Verification code sent to {email}'}), 200


@app.route('/verify-code', methods=['POST'])
def verify_code():
    email = request.json.get('email')
    phone_number = request.json.get('phone_number')
    code = request.json.get('code')

    if email:
        verification = Verification.query.filter_by(email_id=email).order_by(Verification.id.desc()).first()
        user = User.query.filter_by(email=email).first()
        if not verification:
            return jsonify({'error': 'No verification found for the provided email'}), 404
    elif phone_number:
        verification = Verification.query.filter_by(phone_number=phone_number).order_by(Verification.id.desc()).first()
        user = User.query.filter_by(phone=phone_number).first()
        if not verification:
            return jsonify({'error': 'No verification found for the provided phone number'}), 404
    else:
        return jsonify({'error': 'Either email or phone number is required'}), 400

    # Check if the verification code matches
    if verification.code != code:
        return jsonify({'error': 'Verification code does not match'}), 400

    # Check if the verification code has expired
    time_difference = datetime.utcnow() - verification.expires_at
    if time_difference.total_seconds() > 600:  # 600 seconds = 10 minutes
        return jsonify({'error': 'Verification code has expired'}), 400

    # Update the user's isActive flag
    if user:
        user.is_active = True
        db.session.commit()
        return jsonify({'message': 'Verification code has been verified and user is now active'}), 200
    else:
        return jsonify({'error': 'No user found for the provided email'}), 404


@app.route("/report-user", methods=["POST"])
@jwt_required()
def report():
    try:
        if request.json.get('reported_user') is None:
            return jsonify({'error': 'reported_user field is required'}), 400
        reported_userid = request.json.get('reported_user')
        # Check if user with provided username exists
        reported_user_details = User.query.filter_by(
            id=reported_userid, is_active=True).first()
        if not reported_user_details:
            return jsonify({'error': 'Reported user not found'}), 404
        reporting_user = get_jwt_identity()
        existing_combination = Reporting.query.filter_by(
            reporting_user=reporting_user, reported_user=reported_user_details.username).first()
        if existing_combination is not None:
            return jsonify({'error': 'You have already reported this username, sorry !!'}), 400
        try:
            reporting = Reporting(
                reported_user=reported_user_details.username, reporting_user=reporting_user)
            db.session.add(reporting)
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            return jsonify({'Error occured while adding reporting : ': str(e)})
        no_of_reports = reported_user_details.reported_count+1
        reported_user_details.reported_count = reported_user_details.reported_count+1
        if no_of_reports > app.config['REPORTED_COUNT_THRESHOLD']:
            reported_user_details.is_active = False
            body = f"Your profile has been reported more than 5 times. Please contact the admin at megaminds.capstone@gmail.com . Thank you! \n\n"
            msg = Message(subject="Account Deleted After Multiple Reports", recipients=[
                reported_user_details.email], body=body)
            mail.send(msg)
        db.session.commit()
        body = f"The username: {reported_user_details.username} has been reported by the user: {reporting_user} \n\n"
        msg = Message(subject="Account Reported", recipients=[
                      "megaminds.capstone@gmail.com"], body=body)
        mail.send(msg)
        return jsonify(message="The user has been reported"), 200
    except Exception as ex:
        return jsonify({'error': 'Error occured while reporting the user, sorry !!'}), 500
