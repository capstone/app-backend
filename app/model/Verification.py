from app import db
from app.model.user import User

class Verification(db.Model):
    __tablename__ = 'verifications'

    id = db.Column(db.Integer, primary_key=True)
    email_id = db.Column(db.String(120), db.ForeignKey('user.email'))
    email = db.relationship('User', foreign_keys=[email_id], backref='email_verifications')

    phone_number = db.Column(db.String(12), db.ForeignKey('user.phone'))
    phone = db.relationship('User', foreign_keys=[phone_number], backref='phone_verifications')

    code = db.Column(db.String(6), nullable=False)
    expires_at = db.Column(db.DateTime, nullable=False)

    def __init__(self, email=None, phone=None, code=None, expires_at=None, user=None):
        self.email_id = email
        self.phone_number = phone
        self.code = code
        self.expires_at = expires_at
        # self.verification_user = user

    def __repr__(self):
        return f"<Verification: {self.id}>"