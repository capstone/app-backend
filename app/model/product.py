from app import db, ma
from datetime import datetime


class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    description = db.Column(db.Text)
    images = db.Column(db.Text)
    price = db.Column(db.Float, nullable=False)
    category = db.Column(db.String(255))
    condition = db.Column(db.String(255))
    location = db.Column(db.String(255))
    created_date = db.Column(db.DateTime, default=datetime.utcnow)
    market_available_date = db.Column(db.DateTime)
    seller_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    is_active = db.Column(db.Boolean, default=True)
    is_sold = db.Column(db.Boolean, default=False)

    def as_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'description': self.description,
            'images': self.images,
            'price': self.price,
            'category': self.category,
            'condition': self.condition,
            'location': self.location,
            'created_date': self.created_date.isoformat(),
            'market_available_date': self.market_available_date.isoformat() if self.market_available_date else None,
            'seller_id': self.seller_id,
            'is_active': self.is_active,
            'is_sold': self.is_sold
        }

    def __repr__(self):
        return '<Product %r>' % self.name


class ProductSchema(ma.Schema):
    class Meta:
        fields = ('id', 'name', 'description', 'images', 'price', 'category', 'condition', 'location', 'created_date',
                  'market_available_date', 'seller_id', 'is_active', 'is_sold')


productSchema = ProductSchema()
productSchema = ProductSchema(many=True)

# The first instance of ProductSchema is created without any arguments and is used for serializing/deserializing a
# single product. The second instance of ProductSchema is created with the many=True argument and is used for
# serializing/deserializing a list of products.
