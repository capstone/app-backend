from app import db, ma


class Reporting(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    reported_user = db.Column(db.String(64), db.ForeignKey(
        'user.username'), nullable=False)
    reporting_user = db.Column(db.String(64), db.ForeignKey(
        'user.username'), nullable=False)
    __table_args__ = (db.UniqueConstraint(
        'reported_user', 'reporting_user', name='_reported_reporting_uc'),)


class ReportSchema(ma.Schema):
    class Meta:
        model = Reporting
