from app import db, ma

import enum
import json

from app.model.rating import RatingSchema


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True,
                         unique=True, nullable=False)
    email = db.Column(db.String(120), index=True, unique=True, nullable=False)
    phone = db.Column(db.String(12), index=True, unique=True, nullable=True)
    password = db.Column(db.String(128), nullable=False)
    products = db.relationship('Product', backref='user', lazy=True)
    college = db.Column(db.String(250), nullable=False)
    ratings = db.relationship('Rating', backref='user', lazy=True)
    avg_buyer_rating = db.Column(db.Integer, default=0)
    avg_seller_rating = db.Column(db.Integer, default=0)
    profile_image = db.Column(db.String(250))
    is_active = db.Column(db.Boolean, default=False)
    reported_count = db.Column(db.Integer, default=0)
    def __repr__(self):
        return '<User {}>'.format(self.username)

    def as_dict(self):
        exclude = ['password']  # List of columns to exclude
        return {c.name: getattr(self, c.name) for c in self.__table__.columns if c.name not in exclude}


class UserSchema(ma.Schema):
    class Meta:
        fields = ('id', 'username', 'email', 'phone', 'college',
                  'avg_seller_rating', 'avg_buyer_rating', 'profile_image', 'is_active')


userSchema = UserSchema()
usersSchema = UserSchema(many=True)
