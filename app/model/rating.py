from app import db, ma


class Rating(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    buyer_rating_sum = db.Column(db.Integer, nullable=False)
    no_of_buyer_raters = db.Column(db.Integer, nullable=False, default=False)
    seller_rating_sum = db.Column(db.Integer, nullable=False, default=False)
    no_of_seller_raters = db.Column(db.Integer, nullable=False, default=False)
    username = db.Column(db.String(64), db.ForeignKey(
        'user.username'), nullable=False)

    def __repr__(self):
        return f'<Rating {self.id}>'


class RatingSchema(ma.Schema):
    class Meta:
        model = Rating
