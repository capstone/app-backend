from flask import Flask
from app.config import config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_marshmallow import Marshmallow
from flask_jwt_extended import JWTManager
from flask_cors import CORS
from flask_mail import Mail
app = Flask(__name__)
app.config.from_object(config.Config)
CORS(app)
# DB
db = SQLAlchemy(app)  # Connection to database
ma = Marshmallow(app)
jwt = JWTManager(app)
migrate = Migrate(app, db)
mail = Mail(app)


from app.routes import routes
